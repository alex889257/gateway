package com.example.gateway.utils;

import com.example.gateway.models.Rate;
import com.example.gateway.models.Request;
import com.example.gateway.models.TimeReceived;
import com.example.gateway.models.dtos.*;
import com.example.gateway.models.dtos.json_request.CurrentRequestDto;
import com.example.gateway.models.dtos.json_request.HistoryRequestDto;
import com.example.gateway.models.dtos.xml_requests.XmlCommandDto;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Double.parseDouble;
import static java.sql.Timestamp.from;
import static java.time.Instant.ofEpochSecond;

public class DtoEntityMapperUtil {

    public static CurrentRateDto toCurrRateDto(String baseCurrency, Rate rate) {
        return new CurrentRateDto(baseCurrency, rate.getCurrency(), rate.getValue());
    }

    public static CurrentRateDto toCurrRateDto(String baseCurrency, String currency, String rate) {
        return new CurrentRateDto(baseCurrency, currency, parseDouble(rate));
    }

    public static Rate toRate(Map.Entry<String, Double> entry, TimeReceived timeReceived) {
        return new Rate(entry.getKey(), entry.getValue(), timeReceived);
    }

    public static TimeReceived toTimeReceived(RatesDto ratesDto) {
        return new TimeReceived(from(ofEpochSecond(ratesDto.getTimestamp())));
    }

    public static List<Rate> toRates(RatesDto ratesDto, TimeReceived timeReceived) {
        return ratesDto
                .getRates()
                .entrySet()
                .stream()
                .map(entry -> toRate(entry, timeReceived))
                .collect(Collectors.toList());
    }

    public static Request toRequest(CurrentRequestDto dto, String userAgent) {
        return new Request(userAgent, dto.getRequestId(), from(ofEpochSecond(dto.getTimestamp())), dto.getClient());
    }

    public static com.example.gateway.models.dtos.RequestDto toRequestDto(Request request) {
        return new com.example.gateway.models.dtos.RequestDto(request.getServiceName(), request.getRequestId(), request.getTimestamp().toInstant().getEpochSecond(), request.getClientId());
    }

    public static HistoryRateDto toHistoryRateDto(String baseCurrency, String currency, List<Double> rates) {
        return new HistoryRateDto(baseCurrency, currency, rates);
    }

    public static CurrentRequestDto toCurrentRequestDto(XmlCommandDto dto) {
        return new CurrentRequestDto(dto.getRequestId(), Instant.now().getEpochSecond(), dto.getGetDto().getClientId(), dto.getGetDto().getCurrency());
    }

    public static HistoryRequestDto toHistoryRequestDto(XmlCommandDto dto) {
        return new HistoryRequestDto(dto.getRequestId(), Instant.now().getEpochSecond(), dto.getHistory().getClientId(), dto.getHistory().getCurrency(), dto.getHistory().getPeriod());
    }

    public static XmlCurrentRateDto toXmlCurrentRateDto(CurrentRateDto dto) {
        return new XmlCurrentRateDto(dto.getBaseCurrency(), dto.getTargetCurrency(), dto.getRate());
    }

    public static XmlHistoryRateDto toXmlHistoryDto(HistoryRateDto dto) {
        return new XmlHistoryRateDto(dto.getBaseCurrency(), dto.getTargetCurrency(), dto.getRates());
    }
}
