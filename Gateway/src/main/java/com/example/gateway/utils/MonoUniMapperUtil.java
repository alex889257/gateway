package com.example.gateway.utils;

import io.smallrye.mutiny.Uni;
import reactor.core.publisher.Mono;

public class MonoUniMapperUtil {

    public static <T> Mono<T> toMono(Uni<T> uni) {
        return Mono.fromCompletionStage(uni.subscribeAsCompletionStage());
    }

    public static <T> Uni<T> toUni(Mono<T> mono) {
        return Uni.createFrom().publisher(mono);
    }
}
