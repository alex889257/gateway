package com.example.gateway.models.dtos.xml_requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@Getter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "command")
public class XmlCommandDto {

    @XmlAttribute(name = "id")
    private String requestId;

    @XmlElement(name = "get")
    private XmlGetDto getDto;

    @XmlElement(name = "history")
    private XmlHistoryDto history;
}
