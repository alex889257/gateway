package com.example.gateway.models.dtos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "current")
public class XmlCurrentRateDto extends CurrentRateDto {

    public XmlCurrentRateDto() {
        super();
    }

    public XmlCurrentRateDto(String baseCurrency, String targetCurrency, double rate) {
        super(baseCurrency, targetCurrency, rate);
    }
}
