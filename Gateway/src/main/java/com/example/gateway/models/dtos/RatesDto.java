package com.example.gateway.models.dtos;

import lombok.Data;

import java.util.Map;

@Data
public class RatesDto {

    private long timestamp;
    private Map<String, Double> rates;

    public RatesDto() {
    }

    public RatesDto(Long timestamp, Map<String, Double> rates) {
        this.timestamp = timestamp;
        this.rates = rates;
    }
}
