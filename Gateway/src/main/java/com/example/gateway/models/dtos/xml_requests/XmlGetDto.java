package com.example.gateway.models.dtos.xml_requests;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "get")
public class XmlGetDto {

    @XmlAttribute(name = "consumer")
    private String clientId;

    @XmlElement(name = "currency")
    private String currency;
}
