package com.example.gateway.models.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestDto {
    private String serviceName;
    private String requestId;
    private long time;
    private String clientId;

    public RequestDto() {
    }

    public RequestDto(String serviceName, String requestId, long time, String clientId) {
        this.serviceName = serviceName;
        this.requestId = requestId;
        this.time = time;
        this.clientId = clientId;
    }
}
