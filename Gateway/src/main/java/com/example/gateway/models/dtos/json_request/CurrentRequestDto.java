package com.example.gateway.models.dtos.json_request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrentRequestDto {

    @NotNull
    @NotEmpty
    private String requestId;

    @NotNull
    @Positive
    private long timestamp;

    @NotNull
    @NotEmpty
    private String client;

    @NotNull
    @NotEmpty
    private String currency;



}
