package com.example.gateway.models;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(name = "requests")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "serviceName")
    private String serviceName;

    @Column(name = "requestId")
    private String requestId;

    @Column(name = "timestamp")
    private Timestamp timestamp;

    @Column(name = "clientId")
    private String clientId;

    public Request() {
    }

    public Request(String serviceName, String requestId, Timestamp timestamp, String clientId) {
        this.serviceName = serviceName;
        this.requestId = requestId;
        this.timestamp = timestamp;
        this.clientId = clientId;
    }
}
