package com.example.gateway.models.dtos;

import lombok.Data;

@Data
public class CurrentRateDto {
    private String baseCurrency;
    private String targetCurrency;
    private double rate;

    public CurrentRateDto(String baseCurrency, String targetCurrency, double rate) {
        this.baseCurrency = baseCurrency;
        this.targetCurrency = targetCurrency;
        this.rate = rate;
    }

    public CurrentRateDto() {

    }
}
