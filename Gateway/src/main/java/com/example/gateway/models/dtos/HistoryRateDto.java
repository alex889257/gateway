package com.example.gateway.models.dtos;

import lombok.Data;

import java.util.List;

@Data
public class HistoryRateDto {
    private String baseCurrency;
    private String targetCurrency;
    private List<Double> rates;

    public HistoryRateDto() {
    }

    public HistoryRateDto(String baseCurrency, String targetCurrency, List<Double> rates) {
        this.baseCurrency = baseCurrency;
        this.targetCurrency = targetCurrency;
        this.rates = rates;
    }
}
