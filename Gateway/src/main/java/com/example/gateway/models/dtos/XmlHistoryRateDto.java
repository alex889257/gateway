package com.example.gateway.models.dtos;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "history")
public class XmlHistoryRateDto extends HistoryRateDto{

    public XmlHistoryRateDto() {
    }

    public XmlHistoryRateDto(String baseCurrency, String targetCurrency, List<Double> rates) {
        super(baseCurrency, targetCurrency, rates);
    }
}
