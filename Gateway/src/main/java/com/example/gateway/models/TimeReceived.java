package com.example.gateway.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "time_received")
public class TimeReceived {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "time")
    private Timestamp time;

    @OneToMany(mappedBy = "timeReceived", fetch = FetchType.LAZY)
    List<Rate> rates;

    public TimeReceived(Timestamp time) {
        this.time = time;
    }

    public TimeReceived() {
    }
}
