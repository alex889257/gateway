package com.example.gateway.models.dtos.xml_requests;


import lombok.*;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "history")
public class XmlHistoryDto {

    @XmlAttribute(name = "consumer")
    private String clientId;

    @XmlAttribute(name = "currency")
    private String currency;

    @XmlAttribute(name = "period")
    private int period;
}
