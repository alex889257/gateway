package com.example.gateway.models;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "rates")
public class Rate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "currency")
    private String currency;

    @Column(name = "value")
    private double value;

    @ManyToOne
    @JoinColumn(name = "time_received_id")
    private TimeReceived timeReceived;

    public Rate(String currency, double value, TimeReceived timeReceived) {
        this.currency = currency;
        this.value = value;
        this.timeReceived = timeReceived;
    }

    public Rate() {
    }
}
