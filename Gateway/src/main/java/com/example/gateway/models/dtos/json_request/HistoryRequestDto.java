package com.example.gateway.models.dtos.json_request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
public class HistoryRequestDto extends CurrentRequestDto {

    @NotNull
    @Positive
    private int period;

    public HistoryRequestDto() {
    }

    public HistoryRequestDto(String requestId, long timestamp, String client, String currency, int period) {
        super(requestId, timestamp, client, currency);
        this.period = period;
    }
}
