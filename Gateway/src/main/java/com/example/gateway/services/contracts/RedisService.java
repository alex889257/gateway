package com.example.gateway.services.contracts;


import com.example.gateway.models.dtos.CurrentRateDto;
import reactor.core.publisher.Mono;

import java.util.List;

public interface RedisService {

    Mono<Boolean> isDuplicateRequest(String requestId);

    Mono<Void> addRequest(String requestId);

    Mono<String> getCurrentRate(String currency);

    Mono<Void> cacheCurrentRate(CurrentRateDto dto);

    Mono<Void> cacheRatesHistory(String currency, long period, List<Double> rates);

    Mono<List<Double>> getHistoryRates(String currency, long period);

    Mono<Void> clearCache();
}
