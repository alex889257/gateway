package com.example.gateway.services;

import com.example.gateway.models.Request;
import com.example.gateway.services.contracts.RabbitService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.rabbitmq.*;

import javax.annotation.PostConstruct;

import static com.example.gateway.utils.DtoEntityMapperUtil.toRequestDto;

@Service
public class RabbitServiceImpl implements RabbitService {

    private static final String EXCHANGE_TYPE = "direct";
    private static final String EXCHANGE_NAME = "request_exchange";
    private static final String QUEUE_NAME = "request_queue";
    private static final String ROUTING_KEY = "request_key";

    private final Sender sender;
    private final ObjectMapper jsonMapper;

    @Autowired
    public RabbitServiceImpl(Sender sender, ObjectMapper jsonMapper) {
        this.sender = sender;
        this.jsonMapper = jsonMapper;
    }

    @PostConstruct
    public void setup() {
        sender.declareExchange(ExchangeSpecification.exchange(EXCHANGE_NAME).type(EXCHANGE_TYPE))
                .then(sender.declareQueue(QueueSpecification.queue(QUEUE_NAME)))
                .then(sender.bind(BindingSpecification.binding(EXCHANGE_NAME, ROUTING_KEY, QUEUE_NAME)))
                .subscribe();
    }

    @Override
    public Mono<Void> sendMessage(Request request) {
        return Mono.fromCallable(() -> {
                    try {
                        return jsonMapper.writeValueAsString(toRequestDto(request));
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException("Failed to serialize message", e);
                    }
                })
                .map(json -> new OutboundMessage(EXCHANGE_NAME, ROUTING_KEY, json.getBytes()))
                .flatMap(outboundMessage -> sender.send(Mono.just(outboundMessage)))
                .then();
    }
}
