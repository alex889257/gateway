package com.example.gateway.services.contracts;

import com.example.gateway.models.Request;
import reactor.core.publisher.Mono;

public interface RabbitService {
    Mono<Void> sendMessage(Request request);
}
