package com.example.gateway.services;

import com.example.gateway.models.dtos.CurrentRateDto;
import com.example.gateway.services.contracts.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

import static io.netty.util.internal.StringUtil.isNullOrEmpty;
import static java.lang.String.format;
import static java.util.Collections.emptyList;

@Service
public class RedisServiceImpl implements RedisService {

    private static final String DEFAULT_CACHED = "";

    private static final String REQUEST_SET_NAME = "requestIds";
    private static final String CURRENT_RATE_KEY_TEMPLATE = "currencyRate:%s";
    private static final String RATE_HISTORY_KEY_TEMPLATE = "history:%s:%s";
    private static final String RATE_HISTORY_KEYS_PREFIX = "history:*";

    private final ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

    @Autowired
    public RedisServiceImpl(ReactiveRedisTemplate<String, String> reactiveRedisTemplate) {
        this.reactiveRedisTemplate = reactiveRedisTemplate;
    }

    @Override
    public Mono<Boolean> isDuplicateRequest(String requestId) {
        return reactiveRedisTemplate.opsForSet().isMember(REQUEST_SET_NAME, requestId);
    }

    @Override
    public Mono<Void> addRequest(String requestId) {
        return reactiveRedisTemplate.opsForSet().add(REQUEST_SET_NAME, requestId).then();
    }

    @Override
    public Mono<String> getCurrentRate(String currency) {
        return reactiveRedisTemplate.opsForValue().get(format(CURRENT_RATE_KEY_TEMPLATE, currency))
                .defaultIfEmpty(DEFAULT_CACHED);
    }

    @Override
    public Mono<Void> cacheCurrentRate(CurrentRateDto dto) {
        return reactiveRedisTemplate.opsForValue()
                .set(format(CURRENT_RATE_KEY_TEMPLATE, dto.getTargetCurrency()), String.valueOf(dto.getRate()))
                .then();
    }

    @Override
    public Mono<Void> cacheRatesHistory(String currency, long period, List<Double> rates) {
        List<String> stringDoubles = rates.stream()
                .map(String::valueOf)
                .collect(Collectors.toList());
        return reactiveRedisTemplate.opsForList()
                .rightPushAll(format(RATE_HISTORY_KEY_TEMPLATE, currency, period), stringDoubles)
                .then();
    }

    @Override
    public Mono<List<Double>> getHistoryRates(String currency, long period) {
        return findHistoryKey(currency, period)
                .flatMap(key -> {
                    if (isNullOrEmpty(key)) {
                        return Mono.empty();
                    }
                    return reactiveRedisTemplate.opsForList()
                            .range(key, 0, -1)
                            .map(Double::valueOf)
                            .collectList();
                }).defaultIfEmpty(emptyList());
    }

    @Override
    public Mono<Void> clearCache() {
        return reactiveRedisTemplate.keys(format(CURRENT_RATE_KEY_TEMPLATE, "*"))
                .flatMap(reactiveRedisTemplate::delete)
                .then(reactiveRedisTemplate.keys(RATE_HISTORY_KEYS_PREFIX)
                        .flatMap(reactiveRedisTemplate::delete).then());
    }

    private Mono<String> findHistoryKey(String currency, long period) {
        return reactiveRedisTemplate.keys(format(RATE_HISTORY_KEY_TEMPLATE, currency, "*"))
                .filter(key -> {
                    String periodPart = key.substring(format(RATE_HISTORY_KEY_TEMPLATE, currency, "").length());
                    try {
                        int periodNumber = Integer.parseInt(periodPart);
                        return periodNumber >= period;
                    } catch (NumberFormatException e) {
                        return false;
                    }
                }).next()
                .defaultIfEmpty(DEFAULT_CACHED);
    }
}
