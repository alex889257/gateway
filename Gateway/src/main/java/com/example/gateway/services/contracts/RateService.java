package com.example.gateway.services.contracts;

import com.example.gateway.models.dtos.*;
import com.example.gateway.models.dtos.json_request.CurrentRequestDto;
import com.example.gateway.models.dtos.json_request.HistoryRequestDto;
import reactor.core.publisher.Mono;


public interface RateService {

    Mono<CurrentRateDto> getCurrentRate(CurrentRequestDto dto);

    Mono<HistoryRateDto> getHistoryRates(HistoryRequestDto dto);

    Mono<Void> saveRates(RatesDto dto);
}
