package com.example.gateway.services;

import com.example.gateway.exceptions.DuplicateRequestException;
import com.example.gateway.models.Request;
import com.example.gateway.models.dtos.json_request.CurrentRequestDto;
import com.example.gateway.repositories.RequestRepository;
import com.example.gateway.services.contracts.RabbitService;
import com.example.gateway.services.contracts.RedisService;
import com.example.gateway.services.contracts.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import static com.example.gateway.utils.DtoEntityMapperUtil.toRequest;
import static com.example.gateway.utils.MonoUniMapperUtil.toMono;

@Service
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;
    private final RedisService redisService;
    private final RabbitService rabbitService;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository, RedisService redisService,
                              RabbitService rabbitService) {
        this.requestRepository = requestRepository;
        this.redisService = redisService;
        this.rabbitService = rabbitService;
    }

    @Override
    public Mono<Void> handleRequest(CurrentRequestDto dto, String userAgent) {
        Request request = toRequest(dto, userAgent);
        return handleDuplicateRequest(dto.getRequestId())
                .then(toMono(requestRepository.save(request)))
                .then(rabbitService.sendMessage(request));
    }

    private Mono<Void> handleDuplicateRequest(String requestId) throws DuplicateRequestException {
        return redisService.isDuplicateRequest(requestId)
                .handle((exists, sink) -> {
                    if (exists) {
                        sink.error(new DuplicateRequestException());
                    }
                });
    }
}
