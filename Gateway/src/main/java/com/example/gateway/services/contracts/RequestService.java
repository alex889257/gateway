package com.example.gateway.services.contracts;

import com.example.gateway.models.dtos.json_request.CurrentRequestDto;
import reactor.core.publisher.Mono;

public interface RequestService {

    Mono<Void> handleRequest(CurrentRequestDto dto, String userAgent);
}
