package com.example.gateway.services;

import com.example.gateway.models.TimeReceived;
import com.example.gateway.models.dtos.*;
import com.example.gateway.models.dtos.json_request.CurrentRequestDto;
import com.example.gateway.models.dtos.json_request.HistoryRequestDto;
import com.example.gateway.repositories.RateRepository;
import com.example.gateway.repositories.TimeReceivedRepository;
import com.example.gateway.services.contracts.RateService;
import com.example.gateway.services.contracts.RedisService;

import io.smallrye.mutiny.Uni;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.text.DecimalFormat;
import java.time.Instant;
import java.util.stream.Collectors;

import static com.example.gateway.utils.DtoEntityMapperUtil.*;
import static com.example.gateway.utils.MonoUniMapperUtil.toMono;
import static io.netty.util.internal.StringUtil.isNullOrEmpty;
import static java.lang.Double.parseDouble;

@Service
public class RateServiceImpl implements RateService {

    private static final String BASE_CURRENCY = "EUR";
    private static final String NEW_BASE_CURRENCY = "USD";

    private final RateRepository ratesRepository;
    private final TimeReceivedRepository timeReceivedRepository;
    private final RedisService redisService;

    @Autowired
    public RateServiceImpl(RateRepository ratesRepository, TimeReceivedRepository timeReceivedRepository,
                           RedisService redisService) {
        this.ratesRepository = ratesRepository;
        this.timeReceivedRepository = timeReceivedRepository;
        this.redisService = redisService;
    }

    @Override
    public Mono<CurrentRateDto> getCurrentRate(CurrentRequestDto dto) {
        return redisService.getCurrentRate(dto.getCurrency())
                .flatMap(cachedRate -> isNullOrEmpty(cachedRate)
                        ? getCurrentRate(dto.getCurrency())
                        : Mono.just(toCurrRateDto(handleBaseCurrency(dto.getCurrency()), dto.getCurrency(), cachedRate)))
                .flatMap(currentRateDto -> redisService.addRequest(dto.getRequestId())
                        .thenReturn(currentRateDto));
    }

    @Override
    public Mono<HistoryRateDto> getHistoryRates(HistoryRequestDto dto) {
        return redisService.getHistoryRates(dto.getCurrency(), dto.getPeriod())
                .flatMap(cachedRates -> (cachedRates == null || cachedRates.isEmpty())
                        ? getHistoryRate(dto)
                        : Mono.just(toHistoryRateDto(handleBaseCurrency(dto.getCurrency()), dto.getCurrency(), cachedRates.subList(0, dto.getPeriod()))))
                .flatMap(historyRateDto -> redisService.addRequest(dto.getRequestId())
                        .thenReturn(historyRateDto));

    }

    @Override
    public Mono<Void> saveRates(RatesDto ratesDto) {
        return toMono(saveRates(ratesDto, toTimeReceived(ratesDto)));
    }

    private Mono<CurrentRateDto> getCurrentRate(String currency) {
        return toMono(currency.equals(BASE_CURRENCY)
                ? getCurrentRateForBaseCurrency()
                : ratesRepository.findLatestRate(currency).map(rate -> toCurrRateDto(BASE_CURRENCY, rate)))
                .flatMap(currentRateDto -> redisService.cacheCurrentRate(currentRateDto)
                        .thenReturn(currentRateDto));
    }

    private Mono<HistoryRateDto> getHistoryRate(HistoryRequestDto dto) {
        Instant now = Instant.now();
        Instant fromTime = now.minusSeconds(dto.getPeriod() * 3600L);
        return toMono(dto.getCurrency().equals(BASE_CURRENCY)
                ? getHistoryRatesForBaseCurrency(now, fromTime)
                : ratesRepository.findHistoryRates(dto.getCurrency(), now, fromTime)
                .map(historyRateDto -> {
                            historyRateDto.setBaseCurrency(BASE_CURRENCY);
                            historyRateDto.setTargetCurrency(dto.getCurrency());
                            return historyRateDto;
                        }
                )
        ).flatMap(historyRateDto ->
                redisService.cacheRatesHistory(dto.getCurrency(), dto.getPeriod(), historyRateDto.getRates())
                        .thenReturn(historyRateDto));
    }

    private Uni<Void> saveRates(RatesDto ratesDto, TimeReceived timeReceived) {
        return timeReceivedRepository
                .saveTimeReceived(timeReceived)
                .flatMap(savedTimeReceived -> ratesRepository.saveAll(toRates(ratesDto, timeReceived)));
    }

    private String handleBaseCurrency(String currency) {
        return currency.equals(BASE_CURRENCY) ? NEW_BASE_CURRENCY : BASE_CURRENCY;
    }

    private Uni<CurrentRateDto> getCurrentRateForBaseCurrency() {
        return ratesRepository
                .findLatestRate(NEW_BASE_CURRENCY)
                .map(rate -> {
                    rate.setCurrency(BASE_CURRENCY);
                    rate.setValue(reverseRate(rate.getValue()));
                    return toCurrRateDto(NEW_BASE_CURRENCY, rate);
                });
    }

    private Uni<HistoryRateDto> getHistoryRatesForBaseCurrency(Instant now, Instant fromTime) {
        return ratesRepository.findHistoryRates(NEW_BASE_CURRENCY, now, fromTime)
                .map(historyRateDto -> {
                            historyRateDto.setBaseCurrency(NEW_BASE_CURRENCY);
                            historyRateDto.setTargetCurrency(BASE_CURRENCY);
                            historyRateDto.setRates(historyRateDto.getRates()
                                    .stream()
                                    .map(this::reverseRate)
                                    .collect(Collectors.toList()));
                            return historyRateDto;
                        }
                );
    }

    private Double reverseRate(Double rate) {
        DecimalFormat formater = new DecimalFormat("#.######");
        return parseDouble(formater.format(1 / rate));
    }
}
