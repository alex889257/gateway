package com.example.gateway.controllers;

import com.example.gateway.models.dtos.json_request.CurrentRequestDto;
import com.example.gateway.models.dtos.json_request.HistoryRequestDto;
import com.example.gateway.models.dtos.xml_requests.XmlCommandDto;

import com.example.gateway.models.dtos.xml_requests.XmlHistoryDto;
import com.example.gateway.services.contracts.RateService;
import com.example.gateway.services.contracts.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static com.example.gateway.utils.DtoEntityMapperUtil.*;

@RestController
@RequestMapping("/xml_api")
public class XmlController {

    private final RateService ratesService;
    private final RequestService requestsService;

    public XmlController(RateService ratesService, RequestService requestsService) {
        this.ratesService = ratesService;
        this.requestsService = requestsService;
    }

    @PostMapping(path = "/command", consumes = "application/xml", produces = "application/xml")
    public Mono<ResponseEntity<Object>> processXml(@RequestBody Mono<XmlCommandDto> commandMono, @RequestHeader("User-Agent") String userAgent) {
        return commandMono.flatMap(command -> {
            if (command.getGetDto() != null) {
                CurrentRequestDto currentRequestDto = toCurrentRequestDto(command);
                return requestsService.handleRequest(currentRequestDto, userAgent)
                        .then(ratesService.getCurrentRate(currentRequestDto)
                                .map(currentRateDto -> ResponseEntity.ok().body(toXmlCurrentRateDto(currentRateDto))));
            }
            HistoryRequestDto historyRequestDto = toHistoryRequestDto(command);
            return requestsService.handleRequest(historyRequestDto, userAgent)
                    .then(ratesService.getHistoryRates(historyRequestDto)
                            .map(historyRateDto -> ResponseEntity.ok().body(toXmlHistoryDto(historyRateDto))));
        });

    }
}
