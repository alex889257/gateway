package com.example.gateway.controllers;

import com.example.gateway.models.dtos.CurrentRateDto;
import com.example.gateway.models.dtos.HistoryRateDto;
import com.example.gateway.models.dtos.json_request.CurrentRequestDto;
import com.example.gateway.models.dtos.json_request.HistoryRequestDto;
import com.example.gateway.services.contracts.RateService;
import com.example.gateway.services.contracts.RequestService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/json_api", consumes = "application/json", produces = "application/json")
public class JsonController {

    private final RateService ratesService;
    private final RequestService requestsService;

    @Autowired
    public JsonController(RateService ratesService, RequestService requestsService) {
        this.ratesService = ratesService;
        this.requestsService = requestsService;
    }

    @PostMapping("/current")
    public Mono<ResponseEntity<CurrentRateDto>> getCurrentRates(@Validated @RequestBody Mono<CurrentRequestDto> dtoMono,
                                                                @RequestHeader("User-Agent") String userAgent) {
        return dtoMono.flatMap(dto -> requestsService.handleRequest(dto, userAgent)
                .then(ratesService.getCurrentRate(dto)
                        .map(currentRateDto -> ResponseEntity.ok().body(currentRateDto))));
    }

    @PostMapping("/history")
    public Mono<ResponseEntity<HistoryRateDto>> getRatesHistory(@Validated @RequestBody Mono<HistoryRequestDto> dtoMono,
                                                                @RequestHeader("User-Agent") String userAgent) {
        return dtoMono.flatMap(dto -> requestsService.handleRequest(dto, userAgent)
                .then(ratesService.getHistoryRates(dto)
                        .map(historyRateDto -> ResponseEntity.ok().body(historyRateDto))));
    }
}
