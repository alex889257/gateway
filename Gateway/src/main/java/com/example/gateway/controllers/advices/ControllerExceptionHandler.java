package com.example.gateway.controllers.advices;

import com.example.gateway.exceptions.DuplicateRequestException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpStatus.CONFLICT;

@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(DuplicateRequestException.class)
    public Mono<ResponseEntity<String>> handleDuplicateRequestException(DuplicateRequestException e) {
        return Mono.just(ResponseEntity.status(CONFLICT).body(e.getMessage()));
    }
}
