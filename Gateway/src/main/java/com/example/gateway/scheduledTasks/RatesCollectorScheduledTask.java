package com.example.gateway.scheduledTasks;

import com.example.gateway.models.dtos.RatesDto;
import com.example.gateway.services.contracts.RateService;
import com.example.gateway.services.contracts.RedisService;
import com.example.gateway.webclients.contracts.RateWebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;

import static java.util.Collections.emptyMap;

@Component
public class RatesCollectorScheduledTask {

    private static final int TIME_INTERVAL = 60 * 60 * 1000;

    private final RateWebClient webClient;
    private final RateService ratesService;
    private final RedisService redisService;

    @Autowired
    public RatesCollectorScheduledTask(RateWebClient webClient, RateService ratesService, RedisService redisService) {
        this.webClient = webClient;
        this.ratesService = ratesService;
        this.redisService = redisService;
    }

    @PostConstruct
    public void scheduleRatesRetrieval() {
        Mono.defer(this::retrieveRates)
                .repeatWhen(longFlux -> longFlux.delayElements(Duration.ofMillis(TIME_INTERVAL)))
                .subscribe();
    }

    private Mono<Void> retrieveRates() {
        return webClient.getRates()
                .defaultIfEmpty(handleNotChangedRates())
                .flatMap(ratesService::saveRates)
                .then(redisService.clearCache());
    }

    private RatesDto handleNotChangedRates() {
        return new RatesDto(Instant.now().getEpochSecond(), emptyMap());
    }
}
