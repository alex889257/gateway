package com.example.gateway.repositories;

import com.example.gateway.models.Request;
import io.smallrye.mutiny.Uni;
import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RequestRepository {

    private final Mutiny.SessionFactory sessionFactory;

    @Autowired
    public RequestRepository(Mutiny.SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Uni<Void> save(Request request) {
        return sessionFactory.withTransaction(transaction -> transaction.persist(request));
    }
}
