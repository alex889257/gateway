package com.example.gateway.repositories;

import com.example.gateway.models.Rate;
import com.example.gateway.models.dtos.HistoryRateDto;
import io.smallrye.mutiny.Uni;

import javax.persistence.Tuple;

import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class RateRepository {
    private static final String CURRENT_QUERY = "FROM Rate r WHERE r.currency = :currency ORDER BY r.timeReceived.time DESC";
    private static final String HISTORY_QUERY = "SELECT r.value FROM TimeReceived tr" +
            " LEFT JOIN Rate r ON tr.id = r.timeReceived.id AND r.currency = :currency " +
            "WHERE tr.time BETWEEN :toTime AND :fromTime" +
            " ORDER BY tr.time";

    private final Mutiny.SessionFactory sessionFactory;

    @Autowired
    public RateRepository(Mutiny.SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Uni<Rate> findLatestRate(String currency) {
        return sessionFactory
                .withSession(session -> session.createQuery(CURRENT_QUERY, Rate.class)
                        .setParameter("currency", currency)
                        .setMaxResults(1)
                        .getSingleResult());
    }

    public Uni<HistoryRateDto> findHistoryRates(String currency, Instant fromTime, Instant toTime) {
        return sessionFactory.withSession(session -> session.createQuery(HISTORY_QUERY, Double.class)
                .setParameter("currency", currency)
                .setParameter("fromTime", Timestamp.from(fromTime))
                .setParameter("toTime", Timestamp.from(toTime))
                .getResultList()
                .map(this::mapToRates));
    }

    public Uni<Void> saveAll(List<Rate> rates) {
        return sessionFactory.withTransaction(transaction -> transaction.persistAll(rates.toArray()));
    }

    private HistoryRateDto mapToRates(List<Double> rates) {
        double previousValidRate = 0.0;
        List<Double> historyRates = new ArrayList<>();
        HistoryRateDto dto = new HistoryRateDto();
        for (Double currRate : rates) {

            if (currRate == null) {
                currRate = previousValidRate;
            } else {
                previousValidRate = currRate;
            }

            if (currRate != 0) {
                historyRates.add(currRate);
            }
        }
        Collections.reverse(historyRates);
        dto.setRates(historyRates);
        return dto;
    }
}
