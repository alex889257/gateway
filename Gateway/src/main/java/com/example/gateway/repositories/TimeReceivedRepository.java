package com.example.gateway.repositories;


import com.example.gateway.models.TimeReceived;
import io.smallrye.mutiny.Uni;
import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TimeReceivedRepository {

    private final Mutiny.SessionFactory sessionFactory;

    @Autowired
    public TimeReceivedRepository(Mutiny.SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Uni<TimeReceived> saveTimeReceived(TimeReceived timeReceived) {
        return sessionFactory.withTransaction(transaction -> transaction.persist(timeReceived)
                .replaceWith(timeReceived));

    }
}
