package com.example.gateway.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.rabbitmq.client.ConnectionFactory;
import reactor.core.scheduler.Schedulers;
import reactor.rabbitmq.RabbitFlux;
import reactor.rabbitmq.Sender;
import reactor.rabbitmq.SenderOptions;

@Configuration
public class RabbitConfig {

    @Value("${spring.rabbitmq.host}")
    private String rabbitHost;

    @Value("${spring.rabbitmq.port}")
    private int rabbitPort;

    @Bean
    public ConnectionFactory reactiveRabbitConnectionFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitHost);
        factory.setPort(rabbitPort);
        return factory;
    }

    @Bean
    public Sender sender(ConnectionFactory reactiveRabbitConnectionFactory) {
        return RabbitFlux.createSender(new SenderOptions()
                .connectionFactory(reactiveRabbitConnectionFactory)
                .resourceManagementScheduler(Schedulers.boundedElastic()));
    }

}
