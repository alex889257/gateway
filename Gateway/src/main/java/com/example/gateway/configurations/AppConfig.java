package com.example.gateway.configurations;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.config.EnableWebFlux;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Configuration
@EnableScheduling
@EnableWebFlux
public class AppConfig {

    @Bean
    public Mutiny.SessionFactory sessionFactory() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceConfig");
        return emf.unwrap(Mutiny.SessionFactory.class);
    }
}
