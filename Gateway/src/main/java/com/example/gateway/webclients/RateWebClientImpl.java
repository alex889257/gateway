package com.example.gateway.webclients;

import com.example.gateway.models.dtos.RatesDto;
import com.example.gateway.webclients.contracts.RateWebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Optional;
import java.util.function.Function;

import static org.springframework.http.HttpHeaders.*;

@Component
public class RateWebClientImpl implements RateWebClient {

    private static final String LATEST_RATES_END_POINT = "/latest";
    public static final String ACCESS_KEY_PARAM = "access_key";


    private final WebClient webClient;
    private final String accessKey;
    private Optional<String> eTag = Optional.empty();
    private Optional<String> requestDate = Optional.empty();

    @Autowired
    public RateWebClientImpl(Environment environment) {
        String url = environment.getProperty("rates.api.url");
        accessKey = environment.getProperty("rates.api.access-key");
        this.webClient = WebClient
                .builder()
                .baseUrl(url)
                .filter(addEtagFilter())
                .build();
        ;
    }

    @Override
    public Mono<RatesDto> getRates() {
        return webClient
                .get()
                .uri(buildUri(LATEST_RATES_END_POINT))
                .accept(MediaType.APPLICATION_JSON)
                .exchangeToMono(response -> {
                    if (response.statusCode().equals(HttpStatus.NOT_MODIFIED)) {
                        return Mono.empty();
                    }
                    return response.bodyToMono(RatesDto.class);
                });
    }

    private Function<UriBuilder, URI> buildUri(String path) {
        return uriBuilder -> uriBuilder
                .path(path)
                .queryParam(ACCESS_KEY_PARAM, accessKey)
                .build();
    }

    private ExchangeFilterFunction addEtagFilter() {
        return (request, next) -> {
            if (eTag.isPresent() && requestDate.isPresent()) {
                ClientRequest newRequest = ClientRequest
                        .from(request)
                        .headers(httpHeaders -> {
                            httpHeaders.set(IF_NONE_MATCH, eTag.get());
                            httpHeaders.set(IF_MODIFIED_SINCE, requestDate.get());
                        })
                        .build();
                return next.exchange(newRequest);
            }
            return next.exchange(request)
                    .flatMap(response -> {
                                String newEtag = response.headers().asHttpHeaders().getFirst(ETAG);
                                String newLastRequestDate = response.headers().asHttpHeaders().getFirst(DATE);

                                eTag = newEtag != null ? Optional.of(newEtag) : Optional.empty();
                                requestDate = newLastRequestDate != null ? Optional.of(newLastRequestDate) : Optional.empty();

                                return Mono.just(response);
                            }
                    );
        };
    }
}
