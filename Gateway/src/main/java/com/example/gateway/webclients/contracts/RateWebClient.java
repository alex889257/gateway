package com.example.gateway.webclients.contracts;

import com.example.gateway.models.dtos.RatesDto;
import reactor.core.publisher.Mono;

public interface RateWebClient {
    Mono<RatesDto> getRates();
}
