package com.example.gateway.exceptions;

public class DuplicateRequestException extends RuntimeException {

    private static final String DUPLICATE_REQUEST_MESSAGE = "Duplicate request";

    public DuplicateRequestException() {
        super(DUPLICATE_REQUEST_MESSAGE);
    }
}
